
Inductive type : Type :=
| true : type
| false : type
| and : type -> type -> type
| or : type -> type -> type
| imp : type -> type -> type

(* Here we put a in Type instead of type
   because we have no other way to talk about a -> type
   otherwise since term is not yet defined. *)
| full_all (a : Type) : (a -> type) -> type
| full_ex (a : Type) : (a -> type) -> type

(* antiterm ensures that we can assume that
   full_all and full_ex quantify over some (term A) *)
| antiterm : Type -> type.

Fixpoint term (t : type) : Type :=
  match t with
    | true => True
    | false => False
    | and A B => (term A * term B)%type
    | or A B => ((term A) + (term B))%type
    | imp A B => term A -> term B
    | full_all A B => forall x : A, term (B x)
    | full_ex A B => sigT (fun x => term (B x))
    | antiterm A => A
  end.

(* The usual quantifiers, quantifying over types. *)
Definition all (A : type) := full_all (term A).
Definition ex  (A : type) := full_ex  (term A).


Inductive is_ext : forall A, (term A -> type) -> Type :=
| ie_cst A B : is_ext A (fun a => B)
| ie_and_l A B P (b : term B) :
    is_ext (and A B) P -> is_ext A (fun a => P (a, b))
| ie_and_r A B P (a : term A) :
    is_ext (and A B) P -> is_ext B (fun b => P (a, b))
| ie_and A P1 P2 : is_ext A P1 -> is_ext A P2 ->
                   is_ext A (fun a => and (P1 a) (P2 a))
| ie_or_l A B P :
    is_ext (or A B) P -> is_ext A (fun a => P (inl a))
| ie_or_r A B P :
    is_ext (or A B) P -> is_ext B (fun b => P (inr b))
| ie_or A B C P1 P2 f :
      is_ext A P1 ->
      is_ext B P2 ->
      is_ext C
     (fun y : term C =>
      match f y with
      | inl a => P1 a
      | inr b => P2 b
      end)
| ie_antiterm A P : is_ext (antiterm A) P
| ie_ex A B P t1 :
    is_ext (full_ex A B) P ->
    is_ext (B t1) (fun t2 : term (B t1) => P (existT (fun x : A => term (B x)) t1 t2))
| ie_all A B1 P1 :
    (forall b1, is_ext A (P1 b1)) ->
    is_ext A
     (fun y : term A =>
        all B1 (fun a => P1 a y))
| ie_full_all A B1 P1 :
    (forall b1, is_ext A (P1 b1)) ->
    is_ext A
     (fun y : term A =>
        full_all B1 (fun a => P1 a y))
| ie__antiterm A P : is_ext A (fun a => antiterm (P a)).

(* Equality is defined by pattern-matching over types.
   The equality of ex is a bit tricky, it requires the induction principle (and its computational behaviour) so we define eq and induct mutually. *)

Record eq_structure A : Type :=
  mk_eq_struct {
      eq : term A -> term A -> type;
      refl a : term (eq a a);
      transport a b (P : term A -> type) : is_ext _ P -> term (eq a b) -> term (P a) -> term (P b);
      tr_compute a P IEP Ha : transport a a P IEP (refl a) Ha = Ha
    }.

(* True and False have trivial equalities. *)
Program Definition es_true : eq_structure true :=
  {|
    eq x y := true;
    refl x := I;
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  intros [] [] P _ [] Ha.
  assumption.
Defined.
Next Obligation.
  intros [] P IEP Ha.
  reflexivity.
Defined.

Program Definition es_false : eq_structure false :=
  {|
    eq x y := true;
    refl x := I;
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  intros [].
Defined.
Next Obligation.
  intros [].
Defined.

Program Definition es_and A B (esA : eq_structure A) (esB : eq_structure B) : eq_structure (and A B) :=
  {|
    eq x y := and (esA.(eq A) (fst x) (fst y)) (esB.(eq B) (snd x) (snd y));
    refl x := (esA.(refl A) (fst x), esB.(refl B) (snd x));
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  intros A B esA esB (x1, x2) (y1, y2) P IEP.
  simpl.
  intros (p1, p2) Hx.
  apply (esA.(transport A) x1 y1 (fun y1 => P (y1, y2)) (ie_and_l _ _ _ _ IEP) p1).
  apply (esB.(transport B) x2 y2 (fun y2 => P (x1, y2)) (ie_and_r _ _ _ _ IEP) p2).
  exact Hx.
Defined.
Next Obligation.
  intros A B esA esB (x1, x2) P IEP Ha.
  simpl.
  rewrite (esA.(tr_compute A)).
  rewrite (esB.(tr_compute B)).
  reflexivity.
Defined.

(* For Or, equality and reflexivity are defined by pattern matching.
   We define them separately because Program's pattern-matching is heavy. *)
Definition eq_or A B (esA : eq_structure A) (esB : eq_structure B) (x y : term A + term B) : type :=
  match x, y with
    | inl x, inl y => esA.(eq A) x y
    | inr x, inr y => esB.(eq B) x y
    | _, _ => false
  end.

Definition refl_or A B (esA : eq_structure A) (esB : eq_structure B) (x : term A + term B) : term (eq_or A B esA esB x x) :=
  match x with
    | inl x => esA.(refl A) x
    | inr x => esB.(refl B) x
  end.

Program Definition es_or A B
        (esA : eq_structure A)
        (esB : eq_structure B) : eq_structure (or A B) :=
  {|
    eq := eq_or A B esA esB;
    refl := refl_or A B esA esB;
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  - intros A B esA esB [a | a] [b | b] P IEP Ha.
    + intro p.
      apply (esA.(transport A) a b (fun b => P (inl b)) (ie_or_l _ _ _ IEP) Ha p).
    + destruct Ha.
    + destruct Ha.
    + intro p.
      apply (esB.(transport B) a b (fun b => P (inr b)) (ie_or_r _ _ _ IEP) Ha p).
Defined.
Next Obligation.
  intros A B esA esB [a | a] P IEP Ha; simpl.
  - rewrite (esA.(tr_compute A)).
    reflexivity.
  - rewrite (esB.(tr_compute B)).
    reflexivity.
Defined.

(* Not yet proven because this require functionnal extensionality.
   We should limit the induction principle to extensional predicates (as we do for univalence below) *)
Program Definition es_imp A B (esB : eq_structure B) : eq_structure (imp A B) :=
  {|
    eq f g := all A (fun a => esB.(eq B) (f a) (g a));
    refl f a := esB.(refl B) (f a);
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  admit.
Qed.
Next Obligation.
  admit.
Qed.

(* Not proven for the same reason. *)
Program Definition es_all A B (esB : forall a, eq_structure (B a)) : eq_structure (full_all A B) :=
  {|
    eq f g := full_all A (fun a => (esB a).(eq (B a)) (f a) (g a));
    refl f a := (esB a).(refl (B a)) (f a);
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  admit.
Qed.
Next Obligation.
  admit.
Qed.

Program Definition es_antiterm A : eq_structure (antiterm A) :=
  {|
    eq x y := antiterm (x = y);
    refl x := eq_refl x;
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  intros A a b P IEP p H.
  simpl in p.
  destruct p.
  assumption.
Defined.
Next Obligation.
  intros A a P IEP Ha.
  simpl.
  reflexivity.
Defined.



Program Definition es_full_ex A B (esB : forall a, eq_structure (B a)) : eq_structure (full_ex A B) :=
  {|
    eq s t := ex ((es_antiterm A).(eq _) (projT1 s) (projT1 t))
                 (fun p => (esB _).(eq (B (projT1 t)))
                                 ((es_antiterm A).(transport _) (projT1 s) (projT1 t) B (ie_antiterm _ _) p (projT2 s))
                                 (projT2 t));
    refl := _;
    transport := _;
    tr_compute := _
  |}.
Next Obligation.
  intros A B esB s.
  simpl.
  exists eq_refl.
  simpl.
  apply refl.
Defined.
Next Obligation.
  intros A B esB (s1, s2) (t1, t2) P IEP (p1, p2).
  simpl in *.
  intro Hs.
  apply ((esB t1).(transport _) _ _ (fun t2 => P (existT (fun x : A => term (B x)) t1 t2)) (ie_ex _ _ _ _ IEP) p2).
  destruct p1.
  simpl.
  assumption.
Defined.
Next Obligation.
  intros A B esB (s1, s2) P IEP.
  simpl in P.
  simpl.
  intro.
  rewrite tr_compute.
  reflexivity.
Defined.

Fixpoint es (A : type) {struct A} : eq_structure A :=
  match A as A return eq_structure A with
    | true =>  es_true
    | false => es_false
    | and A B => es_and A B (es A) (es B)
    | or A B => es_or A B (es A) (es B)
    | imp A B => es_imp A B (es B)
    | full_all a B => es_all a B (fun a => es (B a))
    | full_ex A B => es_full_ex A B (fun a => es (B a))
    | antiterm A => es_antiterm A
  end.

Definition Eq A := (es A).(eq A).
Definition Refl A := (es A).(refl A).
Definition Transport A := (es A).(transport A).
Definition Tr_compute A := (es A).(tr_compute A).

Theorem eq_is_equality A x y : term (Eq A x y) -> x = y.
Proof.
  induction A; simpl.
  - destruct x; destruct y; reflexivity.
  - destruct x.
  - destruct x as (xa, xb); destruct y as (ya, yb).
    simpl.
    intros (Hea, Heb).
    assert (xa = ya) as H by intuition.
    destruct H.
    assert (xb = yb) as H by intuition.
    destruct H.
    reflexivity.
  - destruct x as [x | x]; destruct y as [y | y].
    + intro HA.
      apply f_equal.
      intuition.
    + simpl; intuition.
    + simpl; intuition.
    + intro HA.
      apply f_equal.
      intuition.
  - assert (forall (A B : Type) (f g : A -> B), (forall x : A, f x = g x) -> f = g) as fun_ext by admit.
    intros H.
    apply fun_ext.
    intros z.
    apply IHA2.
    apply H.
  - assert (forall (A : Type) (B : A -> Type) (f g : forall (t : A), B t), (forall x : A, f x = g x) -> f = g) as fun_ext by admit.
    intros H1.
    apply fun_ext.
    intros z.
    apply H.
    apply H1.
  - destruct x as (s1, s2); destruct y as (t1, t2).
    simpl.
    intros (H1, H2).
    destruct H1.
    simpl in H2.
    apply f_equal.
    apply H.
    apply H2.
  - intuition.
Defined.

Theorem Sym {A a b} : term (Eq A a b) ->
                      term (Eq A b a).
Proof.
  induction A; intros Hab.
  - exact I.
  - exact Hab.
  - destruct a as (a1, a2).
    destruct b as (b1, b2).
    simpl in *.
    split; [apply (IHA1 _ b1) | apply (IHA2 _ b2)]; intuition.
  - destruct a as [a | a]; destruct b as [b | b]; simpl in *; [apply IHA1 | destruct Hab | destruct Hab | apply IHA2]; apply Hab.
  - simpl.
    intro x.
    apply (IHA2 _ (b x)); intuition.
    apply Hab.
  - simpl.
    intro x.
    apply (X _ _ (b x)); intuition.
    apply Hab.
  - simpl.
    destruct a as (a1, a2); destruct b as (b1, b2).
    destruct Hab as (Ha, Hb).
    simpl in Ha.
    destruct Ha.
    simpl in Hb.
    simpl.
    exists eq_refl.
    simpl.
    apply X.
    apply Hb.
  - simpl.
    symmetry.
    assumption.
Defined.

Theorem Trans {A a} : forall b c, term (Eq A a b) ->
                                  term (Eq A b c) ->
                                  term (Eq A a c).
Proof.
  induction A; intros b c Hab Hbc.
  - exact I.
  - exact Hab.
  - destruct a as (a1, a2).
    destruct b as (b1, b2).
    destruct c as (c1, c2).
    simpl in *.
    split; [apply (IHA1 _ b1) | apply (IHA2 _ b2)]; intuition.
  - destruct a as [a | a]; destruct b as [b | b]; destruct c as [c | c]; simpl in *; intuition.
    + apply (IHA1 _ b); intuition.
    + apply (IHA2 _ b); intuition.
  - simpl.
    intro x.
    apply (IHA2 _ (b x)); intuition.
    + apply Hab.
    + apply Hbc.
  - simpl.
    intro x.
    apply (X _ _ (b x)); intuition.
    + apply Hab.
    + apply Hbc.
  - destruct a as (a1, a2).
    destruct b as (b1, b2).
    destruct c as (c1, c2).
    destruct Hab as (Hab1, Hab2).
    destruct Hbc as (Hbc1, Hbc2).
    simpl in *.
    destruct Hab1; destruct Hbc1.
    exists eq_refl.
    simpl.
    apply (X _ _ b2).
    + apply Hab2.
    + apply Hbc2.
  - simpl in *.
    congruence.
Defined.

Lemma ie_eq A B f x : is_ext A (fun y : term A => Eq B (f x) (f y)).
Proof.
  generalize A f x; clear A f x.
  induction B; intros A f x.
  - unfold Eq, eq, es, es_true.
    apply ie_cst.
  - unfold Eq, eq, es, es_false.
    apply ie_cst.
  - unfold Eq, eq. simpl.
    apply ie_and.
    + apply (IHB1 _ (fun a => fst (f a))).
    + apply (IHB2 _ (fun a => snd (f a))).
  - unfold Eq, eq. simpl. unfold eq_or.
    destruct (f x).
    + apply (ie_or B1 B2 A (fun a => Eq B1 t a) (fun a => false)).
      * apply (IHB1 B1 (fun a => a)).
      * apply ie_cst.
    + apply (ie_or B1 B2 A (fun a => false) (fun a => Eq B2 t a)).
      * apply ie_cst.
      * apply (IHB2 B2 (fun a => a)).
  - unfold Eq, eq. simpl.
    apply ie_all.
    intro b1.
    apply (IHB2 A (fun x => f x b1)).
  - unfold Eq, eq. simpl.
    apply ie_full_all.
    intro b1.
    apply (X _ A (fun x => f x b1)).
  - unfold Eq, eq. simpl.
    admit.
  - unfold Eq, eq. simpl.
    apply (ie__antiterm A (fun y => f x = f y)).
Defined.

Fixpoint happly {A B : type} (f : term A -> term B) {struct A} : forall x y : term A, term (Eq A x y) -> term (Eq B (f x) (f y)).
Proof.
  intros x y p.
  apply (Transport A x y (fun y => Eq B (f x) (f y)) (ie_eq _ _ _ _) p).
  apply Refl.
Defined.

Definition eqv (A : type) (B : type) :=
  ex (imp A B)
     (fun f =>
        ex (imp B A)
           (fun g =>
              and (all A (fun x => Eq A (g (f x)) x))
                  (all B (fun y => Eq B (f (g y)) y)))).

Lemma eqv_refl A : term (eqv A A).
Proof.
  simpl.
  exists (fun x => x).
  exists (fun x => x).
  split; apply refl.
Defined.

Lemma eqv_sym A B : term (eqv A B) -> term (eqv B A).
Proof.
  intros (f, (g, (H1, H2))).
  exists g; exists f.
  simpl; intuition.
Defined.

Lemma eqv_and A B C D : term (eqv A B) -> term (eqv C D) -> term (eqv (and A C) (and B D)).
Proof.
  intros (fAB, (fBA, (HA, HB))) (fCD, (fDC, (HC, HD))).
  simpl.
  exists (fun ac : term (and A C) => let (a, c) := ac in (fAB a, fCD c)).
  exists (fun bd : term (and B D) => let (b, d) := bd in (fBA b, fDC d)).
  split; intros (a, c); split; simpl.
  - apply HA.
  - apply HC.
  - apply HB.
  - apply HD.
Defined.

Lemma eqv_or A B C D : term (eqv A B) -> term (eqv C D) -> term (eqv (or A C) (or B D)).
Proof.
  intros (fAB, (fBA, (HA, HB))) (fCD, (fDC, (HC, HD))).
  simpl.
  exists (fun ac : term (or A C) => match ac with inl a => inl (fAB a) | inr c => inr (fCD c) end).
  exists (fun bd : term (or B D) => match bd with inl b => inl (fBA b) | inr d => inr (fDC d) end).
  split; intros [a | c]; simpl.
  - apply HA.
  - apply HC.
  - apply HB.
  - apply HD.
Defined.

Lemma eqv_imp A B C D : term (eqv A B) -> term (eqv C D) -> term (eqv (imp A C) (imp B D)).
Proof.
  intros (fAB, (fBA, (HA, HB))) (fCD, (fDC, (HC, HD))).
  simpl.
  exists (fun ac : term (imp A C) => fun b : term B => fCD (ac (fBA b))).
  exists (fun bd : term (imp B D) => fun a : term A => fDC (bd (fAB a))).
  split.
  - intros f a.
    simpl in *.
    apply (Trans (f (fBA (fAB a))) _ (HC _)).
    apply happly.
    apply HA.
  - intros f b.
    simpl in *.
    apply (Trans (f (fAB (fBA b))) _ (HD _)).
    apply happly.
    apply HB.
Defined.

Inductive is_ext_type : (type -> type) -> Type :=
| iet_id : is_ext_type (fun X => X)
| iet_const Y : is_ext_type (fun X => Y)
| iet_and A B : is_ext_type A -> is_ext_type B -> is_ext_type (fun X => and (A X) (B X))
| iet_or A B : is_ext_type A -> is_ext_type B -> is_ext_type (fun X => or (A X) (B X))
| iet_imp A B : is_ext_type A -> is_ext_type B -> is_ext_type (fun X => imp (A X) (B X)).

Definition ua (P : type -> type) (Hie : is_ext_type P) : forall A B, term (eqv A B) -> term (eqv (P A) (P B)).
Proof.
  induction Hie; intros C D Heqv.
  - assumption.
  - apply eqv_refl.
  - apply eqv_and; intuition.
  - apply eqv_or; intuition.
  - apply eqv_imp; intuition.
Defined.

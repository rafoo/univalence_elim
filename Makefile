DKCHECK=dkcheck
CONFL_CHECKER=~/bin/csiho-0.1.1/csiho.sh
DK_CONFL_OPT=-cc $(CONFL_CHECKER)

all:
	$(DKCHECK) -e ua.dk && $(DKCHECK) $(DK_CONFL_OPT) dummy.dk
